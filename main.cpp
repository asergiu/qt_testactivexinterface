#include <QApplication>
#include <QAxFactory>
#include "swidget.h"


QAXFACTORY_BEGIN("{edd3e836-f537-4c6f-be7d-6014c155cc7a}", "{b7da3de8-83bb-4bbe-9ab7-99a05819e201}")
   QAXCLASS(SWidget)
QAXFACTORY_END()


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);

    // started by COM - don't do anything
    if (QAxFactory::isServer())
        return app.exec();


    // started by user
    SWidget sw;
    sw.resize(800,600);

    QAxFactory::startServer();
    QAxFactory::registerActiveObject(&sw);

    sw.show();

    QObject::connect(qApp, SIGNAL(lastWindowClosed()), &sw, SLOT(quit()));

    return app.exec();
}

