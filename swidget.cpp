#include "swidget.h"
#include <QQuickView>
#include <QApplication>
#include <QTimer>


SWidget::SWidget(QWidget *parent) : QWidget(parent)
{
    QQuickView *view = new QQuickView;
    view->setSource(QUrl(QStringLiteral("qrc:/main.qml")));

    QWidget* w = QWidget::createWindowContainer(view, this);
    w->resize(this->size());
}

SWidget::~SWidget()
{

}

void SWidget::quit(){
    QTimer::singleShot(0, qApp, SLOT(quit()));
}
