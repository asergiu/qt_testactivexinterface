#ifndef SWIDGET_H
#define SWIDGET_H

#include <QWidget>

class SWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int positionX READ x WRITE setX)
    Q_PROPERTY(int positionY READ y WRITE setY)
    Q_CLASSINFO("ClassID", "{2b5775cd-79c2-43da-bc3b-b0e8d1e1c4f7}")
    Q_CLASSINFO("InterfaceID", "{2ce1761e-07a9-415c-bd11-0eab2c7283de}")
    Q_CLASSINFO("RegisterObject", "yes")

public:
    explicit SWidget(QWidget *parent = 0);
    ~SWidget();

signals:

public slots:
    void quit();
    void setX(int x1){
        move(x1,y());
    }
    void setY(int y1){
        move(x(),y1);
    }
};

#endif // SWIDGET_H
