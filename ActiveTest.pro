TEMPLATE = app

QT += qml quick widgets axserver

RC_FILE = activetest.rc

SOURCES += main.cpp \
    swidget.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =


HEADERS += \
    swidget.h

target.path = ./
INSTALLS += target
