import QtQuick 2.3

Rectangle {
    visible: true
    width: 360
    height: 360
    color: "lightblue"

    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }

    Text {
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }
}
